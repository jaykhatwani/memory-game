package com.games.memory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class Instructions extends Activity{
	private boolean  continueMusic = true;

	 public void onCreate( Bundle savedInstanceState) {
	      //TODO remove final n bring in intent
	    	
	    	super.onCreate(savedInstanceState);
	        setContentView(R.layout.instructions);
	        
	        findViewById(R.id.back).setOnClickListener(
	                new OnClickListener() {
	            public void onClick(View v) {
	            	 Intent prefIntent = new Intent(Instructions.this,MemoryGame.class);
	                 Instructions.this.finish();
	            	 Instructions.this.startActivity(prefIntent); 
	            }        });

	
	 }
	 @Override
	    protected void onPause() {
	    super.onPause();
	    if (!continueMusic) {
	    MusicManager.pause();
	    }
	    }
	   
	    @Override
	    protected void onResume() {
	    super.onResume();
	    continueMusic = false;
	    MusicManager.start(this, MusicManager.MUSIC_MENU);
	    }
	    
}
