package com.games.memory;


import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class Categories extends Activity {
  	
	private boolean continueMusic = true;
	
    public void onCreate(final Bundle savedInstanceState) {
      
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.levels1);
        String userName=getIntent().getStringExtra("userName");
        int level=getIntent().getIntExtra("levels",0);
        final Intent prefIntent = new Intent(Categories.this,Levels.class);
        prefIntent.putExtra("userName", userName);
        prefIntent.putExtra("levels", level);

		if (level <9){
			findViewById(R.id.vegetables).setEnabled(false);
			findViewById(R.id.cars).setEnabled(false);
			findViewById(R.id.animals).setEnabled(false);
			findViewById(R.id.cards).setEnabled(false);
		}else if (level <18){
			findViewById(R.id.cars).setEnabled(false);
			findViewById(R.id.animals).setEnabled(false);
			findViewById(R.id.cards).setEnabled(false);
		}else if (level <27)
		{
			findViewById(R.id.animals).setEnabled(false);
			findViewById(R.id.cards).setEnabled(false);
		}else if (level <36){
			findViewById(R.id.cards).setEnabled(false);
		}
			
	
        findViewById(R.id.fruits).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("item", 0);
            	Categories.this.finish();
            	Categories.this.startActivity(prefIntent); 
            }
        });
        
        findViewById(R.id.vegetables).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("item", 1);
            	Categories.this.finish();
            	Categories.this.startActivity(prefIntent); 
            }
        });

        findViewById(R.id.cars).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("item", 2);
            	Categories.this.finish();
            	Categories.this.startActivity(prefIntent); 
            }
        });

        findViewById(R.id.animals).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("item", 3);
            	Categories.this.finish();
            	Categories.this.startActivity(prefIntent); 
            }
        });

        findViewById(R.id.cards).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("item", 4);
            	Categories.this.finish();
            	Categories.this.startActivity(prefIntent); 
            }
        });
        
        /*findViewById(R.id.back).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	 Intent prefIntent = new Intent(Categories.this,MemoryGame.class);
                 Categories.this.finish();
            	 Categories.this.startActivity(prefIntent); 
            }        });*/

       
    	 
    }

   
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    }
   
    @Override
    protected void onPause() {
    super.onPause();
    if (!continueMusic) {
    MusicManager.pause();
    }
    }
   
    @Override
    protected void onResume() {
    super.onResume();
    continueMusic = false;
    MusicManager.start(this, MusicManager.MUSIC_MENU);
    }
    
    @Override
    protected void onStart() {
    
    	// TODO Auto-generated method stub
    	super.onStart();
    }
    }
