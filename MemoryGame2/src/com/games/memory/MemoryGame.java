package com.games.memory;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class MemoryGame extends Activity {
	private String userName;
	private String[] names = new String[7];
	private int profileCount=0;
	private boolean continueMusic =true;
	MusicManager musicManager = new MusicManager();
	
    public void onCreate(final Bundle savedInstanceState) {
      
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        
        findViewById(R.id.facebook).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.facebook.com/pages/Memory-Game-I/152315808173571"));
            	MemoryGame.this.startActivity(intent);
            }
        });
        
        findViewById(R.id.start_game).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	showDialog(3);
            	
            }
        });

        findViewById(R.id.instructions).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
                Intent prefIntent = new Intent(MemoryGame.this,Instructions.class);
                MemoryGame.this.finish();
                MemoryGame.this.startActivity(prefIntent); 
            }
        });

        findViewById(R.id.exit).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
                MemoryGame.this.finish();
            }
        });
        
        findViewById(R.id.topscores).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	Intent prefIntent = new Intent(MemoryGame.this,TopScores.class);
                  MemoryGame.this.finish();
            	  startActivity(prefIntent); 
            }
        });
        
    }

   
    
    protected Dialog onCreateDialog(int id) {
		final ProfileManager profileManager = new ProfileManager(getApplicationContext());
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		AlertDialog alert = null;
		//nested switch id..parent switch..
		switch(id){
		case 4:  //creating new profile
			 final EditText input = new EditText(this);
             builder.setView(input);
             builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog, int whichButton) {
                  userName = input.getText().toString().trim();
                  profileManager.saveProfile(userName);
                  dialog.dismiss();

  		    	Intent prefIntent = new Intent(MemoryGame.this,Categories.class);
              	MemoryGame.this.finish();
              	prefIntent.putExtra("userName", userName);
              	MemoryGame.this.startActivity(prefIntent); 
        }
         });

    builder.setNegativeButton("Cancel",
        new DialogInterface.OnClickListener() {
                         public void onClick(DialogInterface dialog, int whichButton) {
                         dialog.cancel();
                         showDialog(3);
                 }
                 });
			 alert = builder.create();
			break;
			
		case 3: //for showing profiles
		
		 Cursor cur = profileManager.getProfiles();
		 
		 int i=0;
		 if(cur.moveToFirst()){
		    while (cur.isAfterLast() == false) {
		        names[i]=(cur.getString(0));
		   	    cur.moveToNext();
		   	    i++;
		    }}
		    cur.close();
		   profileCount=i-1;
		    names[i]="Create new profile";
		    String[] names1 = new String[cur.getCount()+1];
		   for(int j=0;j<names1.length;j++)
			   names1[j]=names[j];
		 builder.setTitle("Choose your profile");
			
		 builder.setSingleChoiceItems(names1, -1, new DialogInterface.OnClickListener() {
		 
			 public void onClick(DialogInterface dialog, int item) {
				 if(item==profileCount+1)
				 {
					showDialog(4); 
					dialog.dismiss();
					
				 }
				 else{
					 userName=names[item];
		    	dialog.dismiss();
		    	Intent prefIntent = new Intent(MemoryGame.this,Categories.class);
            	MemoryGame.this.finish();
            	prefIntent.putExtra("levels",profileManager.getLevel(userName));
            	prefIntent.putExtra("userName", userName);
            	MemoryGame.this.startActivity(prefIntent); 
		    }}
		});
		 alert = builder.create();
			
			break;
			
			
				}
				return alert;
              
  	        }
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    }
   
    @Override
    protected void onPause() {
    super.onPause();
    if (!continueMusic) {
    MusicManager.pause();
    }
    }
   
    @Override
    protected void onResume() {
    super.onResume();
    continueMusic = false;
    MusicManager.start(this, MusicManager.MUSIC_MENU);
    }
    
    @Override
    protected void onStart() {
    MusicManager.start(getApplicationContext(), 0);
    	// TODO Auto-generated method stub
    	super.onStart();
    }
    }
