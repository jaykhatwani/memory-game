package com.games.memory;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProfileManager {

	
    private static final String DATABASE_TABLE_PROFILES = "profiles";
    private static final String DATABASE_TABLE_PROFILES_ID = "id";
    public static final String DATABASE_TABLE_PROFILES_NAME = "name";
    public static final String DATABASE_TABLE_PROFILES_LEVEL="level";
    public static final int TOP_PROFILE_NB = 5;
   
    private Context ctx;
    private ScoreDBHelper helper;
    private SQLiteDatabase database;
    public boolean scoreWasSaved;

    public ProfileManager(Context context) {
        ctx = context;
        helper = new ScoreDBHelper(ctx);
        database = helper.getWritableDatabase();
       // database.execSQL("drop table   " + DATABASE_TABLE_PROFILES);
        database.execSQL("create table if not exists " + DATABASE_TABLE_PROFILES + " ("
                + DATABASE_TABLE_PROFILES_ID
                + " integer, "+DATABASE_TABLE_PROFILES_LEVEL + " integer, "
                + DATABASE_TABLE_PROFILES_NAME + " TEXT); ");
        if(getProfiles().getCount()==0)  saveProfile("Anonymous");
    	
       }
    
    public Cursor getProfiles() {
        return database.query(DATABASE_TABLE_PROFILES,
                new String[] { DATABASE_TABLE_PROFILES_NAME,
                        DATABASE_TABLE_PROFILES_ID,DATABASE_TABLE_PROFILES_LEVEL }, null, null,
                null, null, DATABASE_TABLE_PROFILES_ID + " DESC");
    }

  
    private boolean profileAlreadyinDB(String player,Cursor cur){
    	if(cur.moveToFirst())
 		while (cur.isAfterLast() == false) {
  		  if(cur.getString(0).equals(player)){
  			 return true;
  			 }
  		  cur.moveToNext();
 		}
    	return false;
    }


    private void updateDB(Cursor cur){
    	String[] names = new String[6];
    	int[] levels = new int[6];
    	if(cur.moveToFirst()){
 		while (cur.isAfterLast() == false) {
  		  names[cur.getInt(1)]=cur.getString(0);
  		  levels[cur.getInt(1)]=cur.getInt(2);
  		  cur.moveToNext();
 		}}
    	for(int i =1;i<cur.getCount();i++)
    	{
    		database.execSQL("update profiles set name = '"+names[i+1] +"' where id =" +i);
    		database.execSQL("update profiles set level = "+levels[i+1] +" where id =" +i);
    }
    }
    
    //method used to upgrade the level...
    public void changeLevel(String player,int level){
    	
    	if(getLevel(player)<0)
    		return;
    	
    	database.execSQL("update profiles set level = "+level +" where name = '" +player+"'");
    }
    
    //method used during initialization of game and check in changeLevel...
    public int getLevel(String player){
    	int level = -1;
    	Cursor cur = getProfiles();
    	if(cur.moveToFirst()){
     		while (cur.isAfterLast() == false) {
      		  if(player.equals(cur.getString(0)))
      				  level=cur.getInt(2);
      		  cur.moveToNext();
     		}}
    	cur.close();
    	return level;
    }
    
    
    public long saveProfile(String player) {
    	Cursor cur = getProfiles(); 
    	String[] whereArgs =new String[]{""+cur.getCount()};
    	ContentValues initialValues = new ContentValues();
         initialValues.put(DATABASE_TABLE_PROFILES_NAME, player);
         initialValues.put(DATABASE_TABLE_PROFILES_LEVEL,0);
         initialValues.put(DATABASE_TABLE_PROFILES_ID, cur.getCount()==TOP_PROFILE_NB?TOP_PROFILE_NB:cur.getCount()+1);
    	if(profileAlreadyinDB(player,cur)){
    		initialValues.put(DATABASE_TABLE_PROFILES_ID, cur.getCount());
    		updateDB(cur);
    		cur.close();
    		return database.update(DATABASE_TABLE_PROFILES, initialValues, DATABASE_TABLE_PROFILES_ID + "=?", whereArgs);}
    	else{
    	if(cur.getCount()>=TOP_PROFILE_NB)
    	{
    		updateDB(cur);
    		whereArgs[0]="5";
    		cur.close();
    		return database.update(DATABASE_TABLE_PROFILES, initialValues, DATABASE_TABLE_PROFILES_ID + "=?", whereArgs);
    	}
    	else{
    		cur.close();
    		return database.insert(DATABASE_TABLE_PROFILES, null,
                initialValues);
    	}}
    }		
    
    

    public class ScoreDBHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "data";
        private static final int DATABASE_VERSION = 2;

        public ScoreDBHelper(Context context) {
            super (context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        	 db.execSQL("create table if not exists " + DATABASE_TABLE_PROFILES + " ("
                     + DATABASE_TABLE_PROFILES_ID
                     + " integer, " +DATABASE_TABLE_PROFILES_LEVEL + " integer, "
                     + DATABASE_TABLE_PROFILES_NAME + " TEXT); ");
        	    
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_PROFILES);
            onCreate(db);
        }

    }
}
