package com.games.memory;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class ScoreManager {

    private static final String DATABASE_TABLE_SCORES = "scores";
    private static final String DATABASE_TABLE_SCORES_ID = "id";
    public static final String DATABASE_TABLE_SCORES_NAME = "name";
    public static final String DATABASE_TABLE_SCORES_SCORE = "score";
    public static final int TOP_SCORE_NB = 10;
    private boolean update=false;
    private int lowestScore=0;
    private int primaryID ;
    public int currentScore =0;
    private Context ctx;
    private ScoreDBHelper helper;
    private SQLiteDatabase database;
    public boolean scoreWasSaved;

    public ScoreManager(Context context) {
        ctx = context;
        helper = new ScoreDBHelper(ctx);
        database = helper.getWritableDatabase();
        database.execSQL("create table if not exists " + DATABASE_TABLE_SCORES + " ("
                + DATABASE_TABLE_SCORES_ID
                + " integer primary key autoincrement, "
                + DATABASE_TABLE_SCORES_NAME + " TEXT, "
                + DATABASE_TABLE_SCORES_SCORE + " integer);");
    
    }

    public Cursor getTopScores() {
        return database.query(DATABASE_TABLE_SCORES,
                new String[] { DATABASE_TABLE_SCORES_NAME,
                        DATABASE_TABLE_SCORES_SCORE,DATABASE_TABLE_SCORES_ID }, null, null,
                null, null, DATABASE_TABLE_SCORES_SCORE + " DESC");
    }

    public boolean isTopScore(String player) {
        if (currentScore < 1)
            return false;

        boolean ret;
        Cursor c = getTopScores();
        
        if(scoreAlreadyinDB(c,player))
        	{c.close();
        	return false;
        	}
        if (c.getCount() >= TOP_SCORE_NB)
        {  	ret = currentScore > getLowestScore(c);
        	update=true;
        	} 
        else
            ret = true;
        c.close();

        return ret;
    }
    
    public boolean scoreAlreadyinDB(Cursor cur,String player){
    	boolean flag=false;
    	if(cur.moveToFirst())
 		while (cur.isAfterLast() == false) {
  		  if(cur.getInt(1)==currentScore && cur.getString(0).equals(player))flag=true;
  		  cur.moveToNext();
 		}
 		//cur.close();
    	return flag;
    }

    public int getLowestScore(Cursor cur)
    {// lowestScore = 0;
 	if(cur.moveToFirst())lowestScore=cur.getInt(1);
 		while (cur.isAfterLast() == false) {
  		  if(cur.getInt(1)<lowestScore)
  			  {lowestScore=cur.getInt(1);
  			  primaryID =cur.getInt(2);
  			  }
  		  cur.moveToNext();
 		}
    	return lowestScore;
    	
    }
    public void saveScoreIfTopScore(String player) {
        if (isTopScore(player))
            saveScore(player);
    }

    private long saveScore(String player) {
    	 ContentValues initialValues = new ContentValues();
         initialValues.put(DATABASE_TABLE_SCORES_NAME, player);
         initialValues.put(DATABASE_TABLE_SCORES_SCORE, currentScore);
    	if(update){
    		update=false;
			String[] whereArgs =new String[]{""+primaryID};
			return database.update(DATABASE_TABLE_SCORES, initialValues, DATABASE_TABLE_SCORES_ID + "=?", whereArgs);}
    	else{
        return database.insert(DATABASE_TABLE_SCORES, null,
                initialValues);}
    }

    public class ScoreDBHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "data";
        private static final int DATABASE_VERSION = 2;

        public ScoreDBHelper(Context context) {
            super (context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table if not exists " + DATABASE_TABLE_SCORES + " ("
                    + DATABASE_TABLE_SCORES_ID
                    + " integer primary key autoincrement, "
                    + DATABASE_TABLE_SCORES_NAME + " TEXT, "
                    + DATABASE_TABLE_SCORES_SCORE + " integer);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                int newVersion) {
        //	Log.e("scrmnger","droping the table");
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_SCORES);
            onCreate(db);
        }

    }
}
