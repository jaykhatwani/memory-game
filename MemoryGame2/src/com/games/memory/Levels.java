package com.games.memory;


import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class Levels extends Activity {
  	private boolean continueMusic = true;
  	
    public void onCreate(final Bundle savedInstanceState) {
      
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.levels);
        int level = getIntent().getIntExtra("levels", 0);
        int item = getIntent().getIntExtra("item", 0);
        String userName= getIntent().getStringExtra("userName");
        final Intent prefIntent = new Intent(Levels.this,CoreGame.class);
        prefIntent.putExtra("item",item);
        prefIntent.putExtra("userName", userName);
        Log.e("levels",""+level);
        int check = level%9;
        //awesome check for displaying level at category level..the gist of level..
        if(level<(item+1)*9)
        {
        switch(check)
        {case 0:
        	findViewById(R.id.two).setEnabled(false);
        	findViewById(R.id.three).setEnabled(false);
        	findViewById(R.id.four).setEnabled(false);
        	findViewById(R.id.five).setEnabled(false);
        	findViewById(R.id.six).setEnabled(false);
        	findViewById(R.id.seven).setEnabled(false);
        	findViewById(R.id.eight).setEnabled(false);
        	findViewById(R.id.nine).setEnabled(false);
        	break;
        case 1:
        	findViewById(R.id.three).setEnabled(false);
        	findViewById(R.id.four).setEnabled(false);
        	findViewById(R.id.five).setEnabled(false);
        	findViewById(R.id.six).setEnabled(false);
        	findViewById(R.id.seven).setEnabled(false);
    		findViewById(R.id.eight).setEnabled(false);
    		findViewById(R.id.nine).setEnabled(false);
    		break;
        case 2:
        	findViewById(R.id.four).setEnabled(false);
        	findViewById(R.id.five).setEnabled(false);
        	findViewById(R.id.six).setEnabled(false);
        	findViewById(R.id.seven).setEnabled(false);
    		findViewById(R.id.eight).setEnabled(false);
    		findViewById(R.id.nine).setEnabled(false);
        	break;
        case 3:
        	findViewById(R.id.five).setEnabled(false);
        	findViewById(R.id.six).setEnabled(false);
        	findViewById(R.id.seven).setEnabled(false);
    		findViewById(R.id.eight).setEnabled(false);
    		findViewById(R.id.nine).setEnabled(false);
        	break;
        case 4:
        	findViewById(R.id.six).setEnabled(false);
        	findViewById(R.id.seven).setEnabled(false);
    		findViewById(R.id.eight).setEnabled(false);
    		findViewById(R.id.nine).setEnabled(false);
        	break;
        case 5:
        	findViewById(R.id.seven).setEnabled(false);
    		findViewById(R.id.eight).setEnabled(false);
    		findViewById(R.id.nine).setEnabled(false);
        	break;
        case 6:
    		findViewById(R.id.eight).setEnabled(false);
    		findViewById(R.id.nine).setEnabled(false);
        	break;
        case 7:
    		findViewById(R.id.nine).setEnabled(false);
        	break;
        	default:
        		break;
        }
        }
        findViewById(R.id.one).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 0);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });
        
        findViewById(R.id.two).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 1);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });

        findViewById(R.id.three).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 2);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });

        findViewById(R.id.four).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 3);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });

        findViewById(R.id.five).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 4);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });
    	 
        findViewById(R.id.six).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 5);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });
    	 
        findViewById(R.id.seven).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 6);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });
    	 
        findViewById(R.id.eight).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 7);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });
    	 
        findViewById(R.id.nine).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	prefIntent.putExtra("level", 8);
            	Levels.this.finish();
            	Levels.this.startActivity(prefIntent); 
            }
        });
        
      /*  findViewById(R.id.back).setOnClickListener(
                new OnClickListener() {
            public void onClick(View v) {
            	 Intent prefIntent = new Intent(Levels.this,Categories.class);
                 Levels.this.finish();
            	 Levels.this.startActivity(prefIntent); 
            }        });*/

    }

   
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    }

    @Override
    protected void onPause() {
    super.onPause();
    if (!continueMusic) {
    MusicManager.pause();
    }
    }
   
    @Override
    protected void onResume() {
    super.onResume();
    continueMusic = false;
    MusicManager.start(this, MusicManager.MUSIC_MENU);
    }
    
    @Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    }
    }
