package com.games.memory;

import com.games.items.Animals;
import com.games.items.Cards;
import com.games.items.Cars;
import com.games.items.Fruits;
import com.games.items.Items;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class CoreGame extends Activity{
	int gridsize= 22;
	static final int MAX_GRIDSIZE=24;
	private int openKey=gridsize +1;
    private ImageView iv1;
    private int openKeyCount=0;
    private ImageAdapter ia = new ImageAdapter(this);
	private GridView gridview;
	private Integer[] fruitGrid = new Integer [MAX_GRIDSIZE];
	private int[] dynamicFruitGrid = new int[MAX_GRIDSIZE]; // to prevent clicking on open items
	private Score score = new Score();
	private boolean lucky= false;
	private TextView mInfoView;
	Items items;
	int level =1;
	private String userName = "Anonymous";
	MediaPlayer mp;
 
	 
	 public void onCreate( Bundle savedInstanceState) {
		 
		 	mp = MediaPlayer.create(getApplicationContext(), R.raw.splat);
		 	mp.setLooping(false);
		 	mp.setVolume(500, 500);
	    	
		 	super.onCreate(savedInstanceState);
	    	setContentView(R.layout.main);
	        mInfoView = (TextView) findViewById(R.id.textview);
	     	gridview = (GridView) findViewById(R.id.gridview);
	     	
	     
	     	
	     //	showDialog(1); //for category
	   
	     	gridview.setOnItemClickListener(new OnItemClickListener() {
	         	
	         	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	         	
	         		lucky = false;
	         		ImageView iv = (ImageView)v;
	         		Animation anim1 = AnimationUtils.loadAnimation(CoreGame.this, R.anim.animation);
	         		iv.startAnimation(anim1);
	         		iv.setImageResource(fruitGrid[position]);
	         		mp.start();
	    	     	      
	         		
	             if(openKey <gridsize && dynamicFruitGrid[position]==-1){
	            	 lucky=true;
	             }
	             if(openKey!=position && dynamicFruitGrid[position]!=1){ //to prevent double clicking
	              dynamicFruitGrid[position]=1;
	         	 openKeyCount++;
	             if(openKey < gridsize){
	             	if(!fruitGrid[openKey].equals(fruitGrid[position]) ){
	             		iv1.setImageResource(R.drawable.circle);
	             		dynamicFruitGrid[openKey]=0;
	             		openKeyCount--;
	             		iv1=iv;
	             		openKey=position;
	             }else{
	    				if(lucky)
	             		score.incLuckyScore();
	    				else
	             		score.incMemScore();
	    				mInfoView.setText("Score : "+ score.getScore()+ "\nMemory points : " + score.getMemScore() + "\nLucky points : "+ score.getLuckyScore());
	             	openKey=gridsize+1; // pair found.reinitialize openKey and iv1
	             	iv1=null;
	             }}
	             else{
	             iv1=iv;	
	             openKey=position;
	             }}

	             if(openKeyCount==((gridsize/2)*2)) {
	             	showDialog(0);
	             	
	             }}
	         });
		 }
	 
		private void initialize(){
			openKeyCount=0;
			openKey=gridsize+1;
			for (int i =0;i < dynamicFruitGrid.length;i++)
	    		  dynamicFruitGrid[i]=-1;
			ia.setThumbsId(gridsize);
	    	gridview.setAdapter(ia);
	    	
		}
		
		@Override
		protected void onPause(){
			super.onPause();
			Intent prefIntent = new Intent(CoreGame.this,MemoryGame.class);
            CoreGame.this.finish();
            CoreGame.this.startActivity(prefIntent); 
      }
		
		@Override
		protected void onStart() {
			 userName = getIntent().getStringExtra("userName");			
			int level=getIntent().getIntExtra("level", -1);
			if(level>-1)
				gridsize= level+12;
			int item=getIntent().getIntExtra("item", -1);
			if	(item> -1)
				setCategory(item);
			// TODO Auto-generated method stub
			super.onStart();
		}
		
		public void setCategory (int item){
			switch (item){
	    	case 0 :
	    		items = new Fruits();	
	        	fruitGrid=items.randomize(gridsize);
	        	initialize();
	        	break;
	    	case 1:
	    		items = new Vegetables();
	        	fruitGrid=items.randomize(gridsize);
	        	initialize();
	    		break;
	    	case 2 :
	    		items = new Cars();
	        	fruitGrid=items.randomize(gridsize);
	        	initialize();
	    		break;
	    	case 3 :
	    		items = new Animals();
	        	fruitGrid=items.randomize(gridsize);
	        	initialize();
	    		break;
	    	case 4 :
	    		items = new Cards();
	        	fruitGrid=items.randomize(gridsize);
	        	initialize();
	    		break;
	    	
	    	}
	   
		}
	 
		protected Dialog onCreateDialog(int id) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			AlertDialog alert = null;
			switch(id){
			case 0: //0 is for the final end dialog
	  	        builder.setMessage("Your Score - " + score.getScore())
	  	               .setCancelable(false)
	  	               .setPositiveButton("Menu", new DialogInterface.OnClickListener() {
	  	                   public void onClick(DialogInterface dialog, int id) {
	  	         	            	Intent prefIntent = new Intent(CoreGame.this,MemoryGame.class);
	  	         	                Bundle bundle = new Bundle();
	  	         	                ScoreManager scoreManager= new ScoreManager(getApplicationContext());
	  	         	                scoreManager.currentScore=score.getScore();
	  	         	             	scoreManager.saveScoreIfTopScore(userName);
	  	         	             	ProfileManager pm = new ProfileManager(getApplicationContext());
	  	         	             	if(getIntent().getIntExtra("level", 0) == pm.getLevel(userName))
	  	         	             	pm.changeLevel(userName,pm.getLevel(userName)+1);
	  	         	                bundle.putSerializable("score", score);
	  	         	            	prefIntent.putExtras(bundle);
	  	         	            	CoreGame.this.finish();
	  	         	            	CoreGame.this.startActivity(prefIntent); 
	  	         	            	mp.stop();
	  	         	            	mp.release();
	  	                   }
	  	               })
	  	               .setNegativeButton("Next Level", new DialogInterface.OnClickListener() {
	  	                   public void onClick(DialogInterface dialog, int id) {
	  	                	   	ProfileManager pm = new ProfileManager(getApplicationContext());
	         	             	if(getIntent().getIntExtra("level", 0) == pm.getLevel(userName))
	         	             	pm.changeLevel(userName,pm.getLevel(userName)+1);
	         	             	gridsize++;
	         	             	fruitGrid = items.randomize(gridsize);
	         	             	initialize();
	  	                   }
	  	               }).setNeutralButton("Replay Again", new DialogInterface.OnClickListener() {
	  	                   public void onClick(DialogInterface dialog, int id) {
	  	                	 fruitGrid=items.randomize(gridsize);
	  	                	 initialize();
	 	                   }
	  	                   });
	  	         alert = builder.create();
	  	         break;
			}
					return alert;
	              
	  	        }
	
}
