package com.games.memory;

import java.io.Serializable;

public class Score implements Serializable {
	
	private int luckyScore=0;
	private int memScore=0;
	
	public int getScore (){
		return luckyScore + memScore;
	}
	
	
	public void incLuckyScore(){
		luckyScore++;
	}
	
	public void incMemScore(){
		memScore=memScore + 2;
	}
	
	public int getLuckyScore() {
		return luckyScore;
	}
	public void setLuckyScore(int luckyScore) {
		this.luckyScore = luckyScore;
	}
	public int getMemScore() {
		return memScore;
	}
	public void setMemScore(int memScore) {
		this.memScore = memScore;
	}
	

}
