package com.games.memory;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class TopScores extends Activity{
	private boolean continueMusic = true;
	
@Override
protected void onStart() {
	// TODO Auto-generated method stub
	Log.e("ts","scm");
	ScoreManager scoreManager =new ScoreManager(getApplicationContext());
	StringBuilder scores=new StringBuilder();
	StringBuilder names=new StringBuilder();
	Cursor cur = scoreManager.getTopScores();
	 	cur.moveToFirst();
    while (cur.isAfterLast() == false) {
        names.append(cur.getString(0)+"\n");
    	scores.append(cur.getString(1)+"\n");
   	    cur.moveToNext();
    }
    cur.close();
	TextView nameView = (TextView)findViewById(R.id.name);
      nameView.setText(names);
      TextView scoreView = (TextView)findViewById(R.id.score);
      scoreView.setText(scores);
        
     super.onStart();
}
	 public void onCreate( Bundle savedInstanceState) {
		 Log.e("ts","oncreate");
				
	    	super.onCreate(savedInstanceState);
	        setContentView(R.layout.topscores);
	        
	     
	        findViewById(R.id.back).setOnClickListener(
	                new OnClickListener() {
	            public void onClick(View v) {
	            		 Intent prefIntent = new Intent(TopScores.this,MemoryGame.class);
	                     TopScores.this.finish();
	            		 TopScores.this.startActivity(prefIntent); 
	            }        });

	
	 }
	 @Override
	    protected void onPause() {
	    super.onPause();
	    if (!continueMusic) {
	    MusicManager.pause();
	    }
	    }
	   
	    @Override
	    protected void onResume() {
	    super.onResume();
	    continueMusic = false;
	    MusicManager.start(this, MusicManager.MUSIC_MENU);
	    }
}
