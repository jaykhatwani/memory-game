package com.games.items;

import java.util.Random;

import android.util.Log;

public class Items {

	private Integer[] items; 
	
	public Integer[] getItems() {
		return items;
	}

	public void setItems(Integer[] items) {
		this.items = items;
	}

	private void randomise(){
		int size = items.length;
		Integer[] temp = items.clone();
		int [] randomInts = new int[size];
		for (int i =0;i <size;i++)
			randomInts[i]=-1;
		
		Random randomGenerator = new Random();
 	 	int randomInt;     
 	 	boolean set;
 	 	for(int i=0;i < size; i++){
 	 		set=true;
 	 		randomInt = randomGenerator.nextInt(size);
 	 		for(int j =0; j<size;j++){
 	 		if(randomInt == randomInts[j])
 	 		{set = false;
 	 			i--;
 	 			break;
 	 		}
 	 		}
 	 		if(set){
 	 		temp[i]=items[randomInt];
 	 		randomInts[i]=randomInt;
 	 		}}
 	 	items=temp;
		
	}
	
	
	public Integer[] randomize(int size){
		randomise();				//to randomly select the values of the items...

		Integer[] itemGrid = new Integer [size];
 		Random randomGenerator = new Random();
 	 	int randomInt;        
 	 	int current =0;
 	 	Boolean flag=true;
 	while (itemGrid[size-1]==null)
 	{  
 		flag = true;
 		randomInt = randomGenerator.nextInt((size+1)/2);//incase u change the grid to odd numbers..change here...
 		for (int check =0;check <current ; check ++)
 		{	
 		if(itemGrid[check]==items[randomInt])
 		{
 			{
 				for (int check2=check+1;check2 <current;check2++)
 				{
 					if(itemGrid[check2]==items[randomInt])
 						{flag=false;
 						 break;
 						}
 				}
 				}
 		}
 		}
 		if(flag)
 		{
 			itemGrid[current]=items[randomInt];
 			current++;
 		}
 	}
 	Log.e("items","randomised"+ itemGrid.length);
	return itemGrid;
 	}
}
